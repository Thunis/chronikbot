#!/usr/bin/env python3

import requests
from urllib.parse import urlencode
import json
from http.cookiejar import CookieJar
import re
from sys import exit, stderr
from functools import total_ordering
import argparse

class RequestError(Exception):
    def __init__(self,err_code,err_headers,err_text,params,data,json):
        super().__init__(err_code,err_headers,err_text,params,data,json)

class Mediawiki:
    def __init__(self, api_url, username, password):
        self._api_url = api_url.rstrip('?')+'?'
        self._lgname = username
        self._lgpassword = password
        self._session = None
    def _login(self):
        self._session = requests.Session()
        params = {"action":"query","meta":"tokens","type":"login"}
        resp = self._request(params)
        token = resp.json()["query"]["tokens"]["logintoken"]
        body = {"lgtoken":token, "lgpassword":self._lgpassword, "lgname":self._lgname}
        params = {"action":"login"}
        resp2 = self._request(params, body)
        if resp2.json()["login"]["result"] != "Success":
            raise Exception("Unexpected login token verification result: Got '%s' instead of 'Success'"%resp2.json()["login"]["result"])
    def _logout(self):
        self.request({"action":"logout"})
    def _request(self,params,data=None,json=None):
        # this changes the params dict!
        params["format"] = "json"
        s = self._api_url+urlencode(params)
        resp = self._session.post(s,data,json)
        if resp.status_code < 200 or resp.status_code >= 300:
            raise RequestError(resp.status_code,resp.headers,resp.text,params,data,json)
        j = resp.json()
        if "warnings" in j:
            print("WARNING:", j, " in Request ", params, file=stderr)
        return resp
    def request(self,params,data=None,json=None):
        resp = None
        lastContinue = {'continue': ''}
        while True:
            paramcopy = dict(params)
            paramcopy.update(lastContinue)
            resp = self._request(paramcopy,data,json)
            resp_json = resp.json()
            if 'error' in resp_json: raise Exception(resp_json['error'])
            if 'warnings' in resp_json: print(resp_json['warnings'], file=stderr)
            if 'query' in resp_json: yield resp_json['query']
            if 'continue' not in resp_json: break
            lastContinue = resp_json['continue']
    def __enter__(self):
        self._login()
        return self
    def __exit__(self, ex_type, exception, traceback):
        # ignore exceptions and just try to quit as cleanly as possible
        self._logout()
        self._session.close()

@total_ordering
class Production:
    def __init__(self, articlename=None, displaytext=None):
        assert articlename is not None or displaytext is not None
        if displaytext is None:
            displaytext = articlename
        self.displaytext = displaytext
        self.articlename = articlename
    def __str__(self):
        if self.articlename is not None:
            # it has a wiki entry
            if self.displaytext == self.articlename:
                return "[[%s]]" % self.articlename
            else:
                return "[[%s|%s]]" % (self.articlename,self.displaytext)
        else:
            return self.displaytext # no wiki link
    def __repr__(self):
        return self.__str__()
    def __lt__(self, other):
        return self.displaytext.lower().__lt__(other.displaytext.lower())
    def __eq__(self, other):
        return self.displaytext.lower() == other.displaytext.lower()

def gen_wiki_text(ytps):
    ret = ""
    for year in sorted(ytps.keys(),reverse=True):
        ret += "=== %d ===\n"%year
        for proj in sorted(ytps[year]):
            ret += "* %s\n"%proj
        ret += "\n"
    return ret.strip()

# gets the first from an iterable and asserts that there are no further elements in it
def fst(it):
    n = 0
    for x in it:
        assert n == 0
        ret = x
        n += 1
    return ret

def categories_with_prefix(wikiconn, prefix):
    ret = []
    for resp in wikiconn.request({"action":"query","list":"allcategories","acprefix":prefix}):
        cats_json = resp
        for cat_json in cats_json["allcategories"]:
            cat = cat_json["*"]
            ret.append(cat)
    return ret

def category_members(wikiconn, category):
    ret = []
    for resp in wikiconn.request({"action":"query","list":"categorymembers","cmtitle":"Category:"+category}):
        ret += resp["categorymembers"]
    return ret

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--dry-run", help="Do not post the updated article to the wiki.", action="store_true")
    argparser.add_argument("--config", help="Alternate config file. Uses \"config.json\" by default.", default="config.json")
    args = argparser.parse_args()
    with open(args.config,"r") as f:
        config = json.load(f)
    try:
        with open(config["additional_chronicle"],"r") as f:
            additional_chronicle = json.load(f)
    except KeyError:
        # no additional_chronicle file specified in config
        additional_chronicle = {}
    years_to_productions = {}
    for n,pl in additional_chronicle.items():
        n = int(n)
        years_to_productions[n] = []
        for p in pl:
            years_to_productions[n].append(Production(displaytext=p))
    with Mediawiki(config["api_url"], config["wiki_username"], config["wiki_password"]) as wikiconn:
        cats = categories_with_prefix(wikiconn, "Spielzeit ")
        cats.sort()
        for cat in cats:
            match = re.match("Spielzeit ([0-9]{4})",cat)
            assert match is not None, "Category \"%s\" does not fit the year regex." % cat
            year = int(match.group(1))
            articles = map(lambda x: x["title"], category_members(wikiconn, cat))
            for article in articles:
                years_to_productions.setdefault(year,[]).append(Production(articlename=article))
        old_site_json = fst(wikiconn.request({"action":"query","prop":"revisions","rvprop":"content|timestamp","titles":"Chronik"}))
        old_site_json = fst(fst(old_site_json["pages"].values())["revisions"])
        timestamp = old_site_json["timestamp"]
        old_wikitext = old_site_json["*"]
        old_matcher = re.match("(?ms).*== Chronik ==(.*)(?:\n== .*)?$",old_wikitext)
        old_chronik = old_matcher.group(1).strip()
        if not re.match("(?:=== [0-9]+ ===\n(?:\* .*\n)+\n*)+",old_chronik):
            print("WARNING: Chronik wasn't generated by me!", file=stderr)
        wikitext = gen_wiki_text(years_to_productions)
        if old_chronik == wikitext:
            print("Chronik already up-to-date.")
            exit(0)
        # need to update content
        new_complete_wiki_text = old_wikitext.replace(old_chronik,"\n"+wikitext+"\n")
        while "== Chronik ==\n\n" in new_complete_wiki_text:
            new_complete_wiki_text = new_complete_wiki_text.replace("== Chronik ==\n\n", "== Chronik ==\n")
        if args.dry_run:
            print(new_complete_wiki_text)
        else:
            csrftoken = fst(wikiconn.request({"action":"query","meta":"tokens"}))["tokens"]["csrftoken"]
            result = list(wikiconn.request({"action":"edit","title":"Chronik","summary":"Chronik automatisch aktualisiert","notminor":1,"bot":1,"basetimestamp":timestamp,"nocreate":1},data={"text":new_complete_wiki_text,"token":csrftoken}))
            # if there was an error in the result, the MediaWiki class would have thrown an exception already
